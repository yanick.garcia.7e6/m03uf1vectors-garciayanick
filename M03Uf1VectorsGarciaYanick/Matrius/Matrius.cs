﻿using System;

namespace Matrius
{
    class Matrius
    {
        
        /*CREADOR: Yanick Garcia
      *FECHA: 24/11/2022
      *METODO: Creando una matriz bidimensional nos encargamos de dejar preparado
            el mapa de juego, en función de los valores que el usuario introduzca el usuario
           será respondido de una entre 3 maneras.*/
        public void SimpleBattleship()
       {
        string[,] Ocean = new string[7, 7] { { "tocado", "tocado", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "tocado" }, { "Solo hay agua...", "Solo hay agua...", "tocado", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "tocado" }, { "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "tocado" }, { "Solo hay agua...", "tocado", "tocado", "tocado", "Solo hay agua...", "Solo hay agua...", "tocado" }, { "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "tocado", "Solo hay agua...", "Solo hay agua..." }, { "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "tocado", "Solo hay agua...", "Solo hay agua..." }, { "tocado", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua...", "Solo hay agua..." } };
            Console.WriteLine("Escriba dos números, el primero para la fila y el segundo para la columna (debe ser un valor entre 0 y 6)");
            Console.Write("- ");
            int Cord1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("- ");
            int Cord2 = Convert.ToInt32(Console.ReadLine());
            if (Cord1 >= 0 && Cord1 <= 6 && Cord2 >= 0 && Cord2 <= 6)
            {
                Console.WriteLine(Ocean[Cord1, Cord2]);
            }
            else
            {
                Console.WriteLine("Error, prueba de nuevo con números válidos.");
            }

        }
       
        /*CREADOR: Yanick Garcia
       *FECHA: 24/11/2022
       *METODO: El programa debe sumar todos los valores de la matriz,
             con el uso de 2 fors es posible especificar las dimensiones de la matriz
            y haciendo uso del contador guardar el valor anterior y sumar el siguiente,
            al terminar cada fila imprimirá el valor actual y despues pasará a la siguiente.*/
        public void MatrixElementSum()
        {
        int count = 0;
            int[,] Matrix = new int[3, 4] { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    count = count += Matrix[i, j];
                }
                Console.WriteLine($"Este es el valor mas el de la fila anterior {count}");
            }
        }
        
        /*CREADOR: Yanick Garcia
       *FECHA: 24/11/2022
       *METODO: Para contar cuantas veces se ha abierto cierta caja lo primero ha sido crear un for que
            guardará cada valor de las filas y el del contador "i", despues dentro de este un if para las columnas
           y en el que además se contarán las veces que se ha abierto y un else if que indica cuando termina.
           Tras el else if con dos for haremos que la consola printe cada fila de la matriz con las veces que se ha abierto cada caja.*/
        public void MatrixBoxesOpenedCounter()
       {
        int[,] boxes = new int[4, 4] { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };
            Console.WriteLine("Introduzca 1 número, presione enter e introduzca el siguiente, si pone -1 finaliza el programa.");
            for (int i = 1; i > 0; i++)
            {
                int BoxPosit1 = Convert.ToInt32(Console.ReadLine());
                i = BoxPosit1;
                if (BoxPosit1 != -1)
                {
                    int BoxPosit2 = Convert.ToInt32(Console.ReadLine());
                    boxes[BoxPosit1, BoxPosit2] = boxes[BoxPosit1, BoxPosit2] + 1;
                }
                else if (BoxPosit1 == -1)
                {
                    Console.WriteLine("\nFinalizado.");
                }
                Console.WriteLine("--------------");
            }
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write(boxes[i, j]);
                }
                Console.Write("\n");
            }

        }
        
        /*CREADOR: Yanick Garcia
        *FECHA: 24/11/2022
        *METODO: El programa analizará los números dentro de la matriz y una vez hecho dirá para  cada uno si es o no multiple de 13.*/
        public void MatrixThereADiv13()
       {
        bool DivOrNot = false;
            int[,] matrix13 = new int[3, 4] { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 62 } };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    int Result = matrix13[i, j] % 13;
                    if (Result == 0)
                    {
                        DivOrNot = true;
                        Console.WriteLine(DivOrNot);
                    }
                    else
                    {
                        DivOrNot = false;
                        Console.WriteLine(DivOrNot);
                    }
                }
            }
        }
        
        /*CREADOR: Yanick Garcia
         *FECHA: 24/11/2022
         *METODO: Los primeros for funcionarán como un bucle para que el programa compare todos los valores dentro de la matriz,
          después dentro colocamos dos if para guardar el valor más alto de la montaña y los de las coordenadas.*/
        public void HighestMountainOnMap()
        {
            double xCord = 0;
            double yCord = 0;
            double max = 0;
            double[,] map = new double[5, 5] { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            for (int x = 0; x < 5; x++)
            {
                for (int y = 0; y < 5; y++)
                {
                    if (max < map[x, y])
                    {
                        max = map[x, y];
                        if (xCord < x || yCord < y)
                        {
                            xCord = x;
                            yCord = y;
                        }
                    }
                }
            }
            Console.WriteLine($"La montaña más alta mide {max} metros y esta en las coordenadas x:{xCord} e y:{yCord}");
        }
       
        /*CREADOR: Yanick Garcia
             *FECHA: 29/11/2022
             *METODO: Para este ejercicio lo primero ha sido coger nuevamente el programa anteriormente creado y añadir
             un segundo bucle de fors que se encargue de establecer los nuevos valores de la matriz y de escribirlos
            ordenadamente en distintas líneas. Para cambiar el valor máximo antes de esto simplemente hacemos que
            se multiplique por 3.2808 en la línea que indica cual es el valor más alto y sus coordenadas.*/
        public void HighestMountainScalechange()
        {   
            double xCord = 0;
            double yCord = 0;
            double max = 0;
            double[,] map = new double[5, 5] { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            for (int x = 0; x < 5; x++)
            {
                for (int y = 0; y < 5; y++)
                {
                    if (max < map[x, y])
                    {
                        max = map[x, y];
                        if (xCord < x || yCord < y)
                        {
                            xCord = x;
                            yCord = y;
                        }
                    }
                }
            }
            Console.WriteLine($"La montaña más alta mide {max * 3.2808} pies y esta en las coordenadas x:{xCord} e y:{yCord}\n");
            for (int x = 0; x < 5; x++)
            {
                for (int y = 0; y < 5; y++)
                {
                    map[x, y] = map[x, y] * 3.2808;
                    Console.Write($"{map[x, y]}; ");
                }
                Console.Write("\n");
            }

        }
        
        /*CREADOR: Yanick Garcia
         *FECHA: 29/11/2022
         *METODO: Usando diversos bucles de for guardamos los valores de ambas matrices y después hacemos que estos se sumen,
         seguidamente los printamos haciendo uso de otro bucle que pase por todos los valores. Por ahora únicamente funciona
         si ambas matrices són del mismo tamaño, en caso de ser la primera matriz más pequeña se suman los valores pero no salen
         el resto.*/
        public void MatrixSum()
        {
            Console.Write("Introduzca el número de filas para la  matriz: ");
            int filaFirst = Convert.ToInt32(Console.ReadLine());
            Console.Write("Ahora el de columnas: ");
            int columnaFirst = Convert.ToInt32(Console.ReadLine());
            int[,] matrixFirst = new int[filaFirst, columnaFirst];
            int[,] matrixResul = new int[filaFirst,columnaFirst];
            Console.WriteLine("Ahora los valores:");
            for (int x = 0; x < filaFirst; x++)
            {
                for (int y = 0; y < columnaFirst; y++)
                {
                    Console.Write("\n- ");
                    matrixFirst[x, y] = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.Write("Introduzca el número de filas para la segunda matriz: ");
            int filaSecond = Convert.ToInt32(Console.ReadLine());
            Console.Write("Ahora el de columnas: ");
            int columnaSecond = Convert.ToInt32(Console.ReadLine());
            int[,] matrixSecond = new int[filaSecond, columnaSecond];
            Console.WriteLine("Ahora los valores:");
            for (int x = 0; x < filaSecond; x++)
            {
                for (int y = 0; y < columnaSecond; y++)
                {
                    Console.Write("\n- ");
                    matrixSecond[x, y] = Convert.ToInt32(Console.ReadLine());
                }
            }
            for (int x = 0; x < filaFirst; x++)
            {
                for (int y = 0; y < columnaFirst; y++)
                {
                    matrixResul[x,y] = matrixFirst[x, y] + matrixSecond[x, y];
                }
            }
            Console.Write("\n");
            for (int x = 0; x < filaFirst; x++)
            {
                for (int y = 0; y < columnaFirst; y++)
                {
                    Console.Write($"{matrixResul[x, y]} ");
                }
                Console.Write("\n");
            }
        }
        public void RookMoves()
        {

        }
        public void MatrixSimetric()
        {

        }
        public void Salir()
        {

        }
        public void Menu()
        {
            Console.WriteLine("1-SimpleBattleship");
            Console.WriteLine("2-MatrixElementSum");
            Console.WriteLine("3-MatrixBoxesOpenedCounter");
            Console.WriteLine("4-MatrixThereADiv13");
            Console.WriteLine("5-HighestMountainOnMap");
            Console.WriteLine("6-HighestMountainScalechange");
            Console.WriteLine("7-MatrixSum");
            Console.WriteLine("8-RookMoves");
            Console.WriteLine("9-MatrixSimetric");
            Console.WriteLine("0-Salir");
            var opcio = Console.ReadLine();
            switch (opcio)
            {
                case "1":
                    SimpleBattleship();
                    break;
                case "2":
                    MatrixElementSum();
                    break;
                case "3":
                    MatrixBoxesOpenedCounter();
                    break;
                case "4":
                    MatrixThereADiv13();
                    break;
                case "5":
                    HighestMountainOnMap();
                    break;
                case "6":
                    HighestMountainScalechange();
                    break;
                case "7":
                    MatrixSum();
                    break;
                case "8":
                    RookMoves();
                    break;
                case "9":
                    MatrixSimetric();
                    break;
                case "0":
                    Salir();
                    break;
                default:
                    Console.WriteLine("Opción incorrecta");
                    break;
            }
        }
        static void Main(string[] args)
        {
            var menu = new Matrius();
            menu.Menu();
        }
    }
}
