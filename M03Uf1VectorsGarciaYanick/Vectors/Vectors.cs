﻿using System;

namespace Vectors
{
    class Vectors
    {/**DATE:8/11/2022
             * NAME: Yanick Garcia
             * DESCRIPTION: Ejercicios de vectores, haciendo uso de arrays crearemos metodos que requieren de guardar conjuntos de valores.
             */
        public void DayOfWeek() 
        {
            /*METODO: Ejercicio de vectores 1, se introduce un numero y devuelve el dia correspondiente.*/

            string[] days = new string[7] { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
            Console.Write("Introduzca un numero de la semana tomando el Lunes como 0 y llegando hasta el 6: ");
            int i = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Es el dia {days[i]}");
        }
        public void PlayerNumber() 
        {
            /*METODO: El usuario introducirá los 5 numeros de los jugadores y el programa los imprimirá siguiendo el formato indicado.*/
            int[] PNum = new int[5];
            Console.Write("Escriba los 5 numeros a los que corresponde cada jugador: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());
            int d = Convert.ToInt32(Console.ReadLine());
            int e = Convert.ToInt32(Console.ReadLine());
            PNum[0] =a;
            PNum[1] =b;
            PNum[2] =c;
            PNum[3] =d;
            PNum[4] =e;
            Console.WriteLine($"[{PNum[0]},{PNum[1]},{PNum[2]},{PNum[3]},{PNum[4]}]");
        }
        public void CandidatesList() 
        {
            /*METODO: Creamos una lista de politicos a partir de los datos que se introduzcan en el sistema y a continuación los ordenamos según su posición*/
            Console.Write("Escriba el numero de candidatos de la lista: ");
            int i = Convert.ToInt32(Console.ReadLine());
            string[] candidates = new string[i+1];
            Console.WriteLine("Introduzca ahora los candidatos: ");
            string k = "";
            for (int j = 1; j<=i; j++)
            {
                k = Convert.ToString(Console.ReadLine());
                candidates[j] = k;
            }Console.WriteLine("Ahora introduzca las posiciones de la lista que quiera ver aparecer: ");
            int pos = 0;
            while (pos != -1)
                /*9/11/22: Se han hecho ajustes, el uso de esta condición terminará el programa si l es igual a -1, pero mientras no sea así el programa seguirá trabajando*/
            {
                pos = Convert.ToInt32(Console.ReadLine());
                if (pos == -1)
                {
                    Console.WriteLine("Terminado");
                } else Console.WriteLine($"{candidates[pos]}\n");
            } 
            
        }
        public void LetterInWord() 
        {
            /*METODO: Introduce una palabra y de la misma indica la letra de la posición que se te pide*/
            Console.Write("Introduzca una palabra: ");
            string word = Convert.ToString(Console.ReadLine());
            Console.Write("Introduzca la posición de la que quiera conocer la letra: ");
            int posit = Convert.ToInt32(Console.ReadLine());
            /*Ha sido necesario buscar como se usa el comando ToCharArray, posiblemente haya otros metodos pero este me ha parecido más ágil para el ejercicio.
             Con este comando ha sido posible convertir la string en un conjunto de caracteres que tomarian en orden una posición dentro del array generado*/
            char[] let = word.ToCharArray();
            Console.WriteLine(word[posit]);

            
        }
        public void AddValuesToList() 
        {
            /*METODO: Con el uso del for podemos asignar el valor 0 a todos los elementos mientras este se repita.
             Al añadir los nuevos valores dentro del for es posible reescribirlos de nuevo a pesar de que previamente
            se les asigne el valor 0*/
            float[] vect = new float[50];
            for (int pos = 0; pos<=49; pos++)
            {
                vect[pos] = 0.0f;
                vect[0] = 31.0f;
                vect[1] = 56.0f;
                vect[19] = 12.0f;
                vect[49] = 79.0f;
                Console.Write($"{vect[pos]}, ");
            }Console.WriteLine("That's all folks!");
        }
        public void Swap()
        {
            /*Haz añadir valores (en concreto 4) para formar un array del que se invertirá el orden de sus valores.*/
            int[] vect = new int[4];
            Console.WriteLine("Escriba cuatro numeros: ");
            for (int rep = 0; rep < 4; rep++)
            {
                vect[rep] = Convert.ToInt32(Console.ReadLine());
            } Array.Reverse(vect);
            Console.WriteLine($"[{vect[0]}, {vect[2]}, {vect[1]}, {vect[3]}]");
            /*Array.Reverse cambia el orden de nuestros valores, haciendo esto es posible printar los valores sin necesidad de escribirlos a la inversa*/
        }
        public void PushButtonPadlockSimulator() 
        {
            /*METODO: Cada vez que un numero sea seleccionado su valor booleano será cambiado, el objetivo es devolver el estado de cada botón al finalizar el programa.*/
            bool[] state = { false, false, false, false, false, false, false, false };
            int fin = 0;
            Console.WriteLine("Introduzca un valor del 0 al 7, presione enter e introduzca el siguiente, cuando haya terminado ponga -1. ");
            while (fin != -1)
            {
                Console.Write("- ");
                fin = Convert.ToInt32(Console.ReadLine());
                if (fin <= 7 && fin >= 0 && state[fin] == false)
                {
                    state[fin] = true;
                }
                else if (fin <= 7 && fin >= 0 && state[fin] == true)
                {
                    state[fin] = false;
                }
                else if (fin > 7 || fin < -1)
                    /*Se usa el valor de -1 ya que en caso de usar 0 saltaria el mensaje antes de escribir el estado de cada boton.*/
                {
                    Console.WriteLine("Numero Invalido, prueba otra vez");
                }
            } Console.WriteLine($"[{state[0]}, {state[1]}, {state[2]}, {state[3]}, {state[4]}, {state[5]}, {state[6]}, {state[7]}]");

            
        }
        public void BoxesOpenedCounter() 
        {
            /*METODO: Este programa esta pensado para contar el número de veces que una caja del banco es abierta.*/
            int[] state = {0,0,0,0,0,0,0,0,0,0,0};
            int count = 0;
            int caja = 0;
            while (count != -1)
                /*Dentro del while se han puesto ciertas condiciones para aumentar el contador de veces que se ha abierto una caja o permitir introducir otra caja en caso de añadir una erronea.*/
            {
                Console.Write("Introduzca la caja que ha sido abierta, introduzca -1 cuando termine: ");
                count = Convert.ToInt32(Console.ReadLine());
               if (count <= 10 && count >= 0)
                {
                    caja = count;
                    state[caja]++;
                }
               else if (count >7 || count < -1)
                {
                    Console.WriteLine("La caja no es parte del banco, elija otra por favor.");
                }
            }Console.WriteLine($"{state[0]}, {state[1]}, {state[2]}, {state[3]}, {state[4]}, {state[5]}, {state[6]}, {state[7]}, {state[8]}, {state[9]}, {state[10]}");
        }
        public void MinOf10Values() 
            /*METODO:Busca el menor valor de entre los que el usuario ha introducido.*/
        {
            int[] value = new int[10];
            Console.WriteLine("Escribe 10 numeros enteros:");
            for (int count = 0; count <10; count++)
            {
                Console.Write($"{count+1} -> ");
                int num = Convert.ToInt32(Console.ReadLine());
                value[count] = num;
            }
            var min = value[0];
            for (int i = 1; i < value.Length; i++)
            {
                if (min > value[i])
                {
                    min = value[i];
                }

            }
            Console.Write($"El numero de menor valor es: {min}");
        }
        public void IsThereAMultipleOf7() 
            /*METODO: El programa busca en la lista de valores especificada si hay un valor multiple de 7, en caso de ser así dirá que es cierto, en caso contrario escribirá false.*/
        {
            int[] values;
            values = new int[] { 4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98, 54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848 };
            bool multiple = false;
            foreach (int num in values)
            {
                if (num % 7 == 0)
                {
                    multiple = true;
                }
            }
            if (multiple == true)
            {
                Console.WriteLine(multiple);
            }
            else if (multiple == false)
            {
                Console.WriteLine(multiple);
            }
        }
        public void SearchInOrdered()
        {
            /*METODO: El usuario especificará el largo de una lista, a continuación
            introducirá unos números y por último un valor que en caso de encontrarse 
            dentro de la lista será identificado por el programa y la consola printara true
            en caso contrario printara false.*/

            Console.Write("Introduzca la cantidad de valores que quiere poner: ");
            int cant = Convert.ToInt32(Console.ReadLine());
            bool Listed = false;
            int[] cantNum = new int[cant];
            Console.WriteLine("Ahora escriba sus numeros:");
            for (int i=0; i<cant; i++)
            {
                Console.Write("- ");
                int list = Convert.ToInt32(Console.ReadLine());
                cantNum[i] = list;
            }
            Console.Write("Por último escoja el valor que quiera buscar en la lista: ");
            int wanted = Convert.ToInt32(Console.ReadLine());
            for (int j = 0; j< cantNum.Length; j++)
            {
                if (wanted == cantNum[j])
                {
                    Listed = true;
                }
            }Console.WriteLine(Listed);
        }
        public void InverseOrder() 
        {
            /*METODO: El usuario introducirá 10 numeros a su gusto y a continuación la consola los devolverá en el orden inverso al de introducción.*/
            int[] disorder = new int[10];
            Console.WriteLine("Por favor introduzca 10 números enteros:");
            for (int i = 0; i < disorder.Length; i++)
            {
                Console.Write("-> ");
                disorder[i] = Convert.ToInt32(Console.ReadLine());
            }Array.Reverse(disorder);
            for (int i = 0; i < disorder.Length; i++)
            {
                Console.WriteLine($"{disorder[i]}.");
            }
        }
        public void Palindrome() 
        {
            /*METODO: El programa debe analizar la palabra y decir si es o no un palindromo.
              La mejor manera de hacerlo a mi parecer ha sido convertir cada caracter
             en un elemento del array let y despues guardar su valor en otro array e invertirlo
             para comparar y determinar si es o no un palindromo*/
            bool BeNot2Be = false;
            Console.Write("Por favor escriba la palabra la cual quiera saber si es o no un palindromo: ");
            string palindOrNot = Convert.ToString(Console.ReadLine());
            char[] let = palindOrNot.ToCharArray();
            char[] palindLet = let;
            Array.Reverse(palindLet);
            Console.WriteLine(palindLet);
            foreach (char letpal in palindOrNot)
            {
                int i = 0;
                if (palindOrNot[i] == let[i])
                {
                    BeNot2Be = true;
                }
                i++;
            }
            Console.WriteLine($"\nLa palabra {palindOrNot} es {BeNot2Be} que es palindromo");

        }
        public void ListSortedValues() 
        {
            /*METODO: La idea es hacer que el siguiente se compare con el maximo valor actual para determinar si esta o no ordenado*/
            bool dis_order = false;
            int count = 0;
            Console.Write("Por favor introduzca el tamaño de la lista: ");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] lista = new int[size];
            Console.WriteLine("Ahora los números de la lista:");
            while (count < size)
            {
                Console.Write("- ");
                lista[count] = Convert.ToInt32(Console.ReadLine());
                while (count < size)
                {
                    if (lista[count] >= lista[int.MaxValue])
                    {
                        dis_order = true;
                    }
                    else
                    {
                        dis_order = false;
                    }
                }
                count++;
            }
            if (dis_order == true)
            {
                Console.WriteLine("Ordenado!");
            }
            else
            {
                Console.WriteLine("Desordenado...");
            }
        }
        public void CapICuaValues()
        {
            Console.Write("Por favor introduzca la cantidad de números que introducirá: ");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] lista = new int[size];
            Console.WriteLine("Ahora los números:");
            for(int i = 0; i<lista.Length; )
            {
                Console.Write("- ");
                lista[i] = Convert.ToInt32(Console.ReadLine());
            } int[] list2 = lista;
            Array.Reverse(list2);
            if (list2[0] == lista[0])
            {
                Console.WriteLine("Cap i cua");
            }else
            {
                Console.WriteLine("No es cap i cua");
            }
        }
        public void ListSameValues() 
        {
        
        }
        public void ListSumValues() 
        {
        
        }
        public void IvaPrices() 
        {
        
        }
        public void CovidGrowRate() 
        {
        
        }
        public void BicicleDistance() 
        {
        
        }
        public void ValuesNearAvg() 
        {
        
        }
        public void Isbn() 
        {
        
        }
        public void Isbn13() 
        {
        
        }
        public void Salir() 
        {
        
        }
        public void Menu()
        {
            Console.WriteLine("1-DayOfWeek");
            Console.WriteLine("2-PlayerNumber");
            Console.WriteLine("3-CandidatesList");
            Console.WriteLine("4-LetterInWord");
            Console.WriteLine("5-AddValuesToList");
            Console.WriteLine("6-Swap");
            Console.WriteLine("7-PushButtonPadlockSimulator");
            Console.WriteLine("8-BoxesOpenedCounter");
            Console.WriteLine("9-MinOf10Values");
            Console.WriteLine("a-IsThereAMultipleOf7");
            Console.WriteLine("b-SearchInOrdered");
            Console.WriteLine("c-InverseOrder");
            Console.WriteLine("d-Palindrome");
            Console.WriteLine("e-ListSortedValues");
            Console.WriteLine("f-CapICuaValues");
            Console.WriteLine("g-ListSameValues");
            Console.WriteLine("h-ListSumValues");
            Console.WriteLine("i-IvaPrices");
            Console.WriteLine("j-CovidGrowRate");
            Console.WriteLine("k-BicicleDistance");
            Console.WriteLine("l-ValuesNearAvg");
            Console.WriteLine("m-Isbn");
            Console.WriteLine("n-Isbn13");
            Console.WriteLine("0-Salir");
            Console.WriteLine("Escoja una opcion");
            var opcio = Console.ReadLine();
            switch (opcio)
            {
                case "1": DayOfWeek();
                    break;
                case "2": PlayerNumber();
                    break;
                case "3": CandidatesList();
                    break;
                case "4": LetterInWord();
                    break;
                case "5": AddValuesToList();
                    break;
                case "6": Swap();
                    break;
                case "7": PushButtonPadlockSimulator();
                    break;
                case "8": BoxesOpenedCounter();
                    break;
                case "9": MinOf10Values();
                    break;
                case "a": IsThereAMultipleOf7();
                    break;
                case "b": SearchInOrdered();
                    break;
                case "c": InverseOrder();
                    break;
                case "d": Palindrome();
                    break;
                case "e": ListSortedValues();
                    break;
                case "f": CapICuaValues();
                    break;
                case "g": ListSameValues();
                    break;
                case "h": ListSumValues();
                    break;
                case "i": IvaPrices();
                    break;
                case "j": CovidGrowRate();
                    break;
                case "k": BicicleDistance();
                    break;
                case "l": ValuesNearAvg();
                    break;
                case "m": Isbn();
                    break;
                case "n": Isbn13();
                    break;
                case "0": Salir();
                    break;
                default: Console.WriteLine("Opción incorrecta");
                    break;
            }
        }
        static void Main(string[] args)
        {
            var menu = new Vectors();
            menu.Menu();
        }
    }
}
